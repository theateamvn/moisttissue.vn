function doHeaderScroll() {
    var o = $("header");
    $(window).scrollTop() >= o.height() ? o.addClass("scrolled") : o.removeClass("scrolled")
}
function smoothScroll(){
  var $window = $(window);
  var scrollTime = 1;
  var scrollDistance = 350;
  var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
  if (is_chrome) {
    $window.on("mousewheel DOMMouseScroll", function(event){
      event.preventDefault();
      var delta = event.originalEvent.wheelDelta/120 || -event.originalEvent.detail/3;
      var scrollTop = $window.scrollTop();
      var finalScroll = scrollTop - parseInt(delta*scrollDistance);
      TweenMax.to($window, scrollTime, {
        scrollTo : { y: finalScroll, autoKill:true },
          ease: Power1.easeOut,
          overwrite: 5
        });

    });
  }
}
$(function() {
    /*ADD SMOOTHSCROLL*/
    smoothScroll();
    $('.menu-item-has-children').append('<span class="submenu-toggle"></span>');
    $('.submenu-toggle').on('touchend',function(){
        $(this).toggleClass('active');
        $(this).parents('.menu-item-has-children').find('.sub-menu').slideToggle();
    });
    $("#button-menu").on("click", function() {
        $(this).toggleClass("active"), $("header").toggleClass("active"), $(".nav-section").toggleClass("nav-active")
    }), $(window).scroll(function() {
        $(window).width() > 768 && doHeaderScroll()
    });
    $(window);
    $(".search-btn").on("click touchend", function() {
        $(".search-box").addClass("active")
    }), $(document).mouseup(function(o) {
        var e = $(".search-box");
        e.is(o.target) || 0 !== e.has(o.target).length || e.removeClass("active")
    })
    /*LOGIN HANDLE*/
    $('a.must-log-in').click(function(e){
      $('#login-modal').modal('show');
    });

    /*SWITCH FORM*/
    $('.login-btn').on('click',function(){
        $('.register-btn').removeClass('active');
        $(this).addClass('active');
        $('#login-form').show();
        $('#register-form').hide();
        $('#login-form').find('input[type="text"]').removeAttr('disabled');
        $('#login-form').find('input[type="password"]').removeAttr('disabled');
        $('#register-form').find('input[type="text"]').attr('disabled','disabled');
        $('#register-form').find('input[type="password"]').attr('disabled','disabled');
        $('#register-form').find('input[type="email"]').attr('disabled','disabled');
    });
     $('.register-btn').on('click',function(){
        $(this).addClass('active');
        $('.login-btn').removeClass('active');
        $('#register-form').show();
        $('#login-form').hide();
        $('#register-form').find('input[type="text"]').removeAttr('disabled');
        $('#register-form').find('input[type="password"]').removeAttr('disabled');
        $('#register-form').find('input[type="email"]').removeAttr('disabled');
        $('#login-form').find('input[type="text"]').attr('disabled','disabled');
        $('#login-form').find('input[type="password"]').attr('disabled','disabled');
    });
    
    /*VALIDATE LOGIN FORM*/
    $('#login-form').validate({
        rules: {
            login_email: {
              required: true,
            },
            login_password: {
              required: true
            }
        },
        messages: {
            login_email: {
              required: "Vui lòng nhập Email / Tên đăng nhập"
            },
            login_password: {
              required: "Vui lòng nhập mật khẩu"
            }
        }
    });

    /*VALIDATE REGISTER FORM*/
    $('#register-form').validate({
        rules: {
            email: {
              required: true,
              email: true
            },
            password: {
              required: true
            },
            confirm_password: {
              required: true,
              equalTo: "#password"
            },
            phone: {
              required: true,
              minlength: 8
            },
            firstname: {
              required: true
            },
            lastname: {
              required: true
            }
        },
        messages: {
            email: {
              required: "Vui lòng nhập Email",
              email: "Email không đúng định dạng"
            },
            password: {
              required: "Vui lòng nhập mật khẩu"
            },
            confirm_password: {
              required: "Vui lòng nhập mật khẩu xác nhận",
              equalTo: "Mật khẩu và xác nhận mật khẩu chưa đúng"
            },
            phone: {
              required: "Vui lòng nhập SĐT",
              minlength: "Số đt tối thiểu 8 số"
            },
            firstname: {
              required: "Vui lòng nhập họ"
            },
            lastname: {
              required: "Vui lòng nhập tên"
            }
        }
    });
}), $("#scrolltop").hide(), $(window).on("load", function() {
    $(".loading-bg").fadeOut(650)
});