<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package moist
 */

$keyword = get_search_query();

get_header();
?>

<div class="content-wrapper"> 
    <!-- STORY -->
    <section id="search-detail">
        <div class="container">
        	<h3 class="xxl-spacing-top"><?php printf( esc_html__( 'Keyword: %s', 'moist' ),  $keyword ); ?></h3>
        	<?php 
        	    $args = array('posts_per_page' => 10, 'post_type' => 'san-pham', 's' => $keyword, 'orderby' => 'date', 'order' => 'DESC',); 
        	    $posts_array = get_posts( $args ); 
        	?>
        	<div class="search-section">
        		<h3><?php _e('Products','moist'); ?></h3>
        		<div id="products">
        			<?php if(!empty($posts_array)): ?>
    			        <?php  foreach ( $posts_array as $post ) :  setup_postdata( $post ); ?>
    			       <div class="product-item wow fadeInUp" data-wow-delay="<?php echo $delay; ?>s">
	                       <div class="row">
	                           <div class="product-image col-sm-2">
	                               <a href="<?php the_permalink(); ?>">
	                                   <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'md_thumb'); ?>">
	                               </a>
	                           </div>
	                           <div class="product-info col-sm-10">
	                               <h3 itemprop="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
	                                <h4><?php echo get_the_excerpt(); ?></h4>
	                                 <?php if(get_field('dong_goi')): ?>
                                    <p class="description">
                                    <?php _e('Package:','moist'); ?> <?php the_field('dong_goi'); ?>
                                    </p>
                                    <?php endif;?>
                                    <?php if(get_field('kich_thuoc')): ?>
                                    <p class="description">
                                    <?php _e('Dimension:','moist'); ?> <?php the_field('kich_thuoc'); ?>
                                    </p>
                                    <?php endif;?>
                                    <?php if(get_field('so_hop_quy')): ?>
                                    <p class="description">
                                    <?php _e('Number Code:','moist'); ?> <?php the_field('so_hop_quy'); ?>
                                    </p>
                                    <?php endif;?>
                                    <p class="more-detail" itemprop="description">
                                    <?php the_field('more_detail'); ?>
                                    </p>
	                                <div class="rating">
                                        <?php echo do_shortcode( '[ratings]' ); ?>
                                    </div>
	                               <a href="<?php the_permalink(); ?>" class="plink"><?php _e('product information', 'moist'); ?></a>
	                           </div>
	                       </div>
	                       <!-- <a href="#" class="buy-btn"><img src="assets/images/buy_btn.png"></a> -->
	                       <a href="<?php the_field('product_link'); ?>" class="buy-btn buy-btn-custom"><?php _e('BUY NOW', 'moist') ?><i class="fas fa-shopping-cart"></i></a>
	                   </div>
    			        <?php endforeach; wp_reset_postdata(); ?>
        			<?php else: ?>
    				<p><?php _e('No Product Found','moist'); ?></p>
        			<?php endif; ?>
        		</div>
        	</div>
        	<div class="search-section">
        		<h3><?php _e('FAQ','moist'); ?></h3>
        		<?php 
        	    $args = array(
                   'posts_per_page' => $post_per_page,
                   'post_type' => 'faq',
                   's' => $keyword,
                   'orderby' => 'date',
                   'order' =>'DESC'
                ); 
                $posts_array = get_posts( $args );
        		?>
        		<div id="faq-content">
        			<?php if(!empty($posts_array)): ?>

        			    <?php  foreach ( $posts_array as $post ) :  setup_postdata( $post ); ?>
        			    <div class="faq-item wow fadeInUp" data-wow-delay="<?php echo $delay; ?>s">
        			        <?php if($first): $first = 0; ?>
        			        <h3><?php _e('Latest Question', 'moist') ?></h3>
        			        <?php endif; ?>
        			        <div class="q">
        			            <div class="row">
        			                <div class="left-col">
        			                    <div class="avatar" style="background: url(<?php echo THEME_URL; ?>/assets/images/sample.png) no-repeat center;"></div>
        			                </div>
        			                <div class="right-col">
        			                    <h4 class="name"><?php echo get_the_excerpt(); ?></h4>
        			                    <p class="question"><?php the_content(); ?> </p>
        			                </div>
        			            </div>
        			        </div>
        			        <div class="a">
        			            <div class="row">
        			                <div class="left-col">
        			                    <h5><?php _e('Answer :', 'moist'); ?></h5>
        			                </div>
        			                <div class="right-col">
        			                    <p class="answer">
        			                        <?php the_field('answer'); ?>
        			                    </p>
        			                </div>
        			            </div>
        			        </div>
        			    </div>
        			    <?php $delay += 0.1; ?>
        			    <?php endforeach; wp_reset_postdata(); ?>
        			<?php else: ?>
        			<p><?php _e('No Question Found','moist'); ?></p>
        			<?php endif; ?>
        		</div>
        	</div>
        </div>
    </section>
    <!-- .STORY -->
</div>	

<?php
get_footer();
