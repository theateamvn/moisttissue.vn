<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package moist
 */

get_header();
?>

<?php 
	if(is_category( 3 )): 
		get_template_part( 'template-parts/cat', 'story' );
	elseif(is_category( 4 )): 
		get_template_part( 'template-parts/cat', 'news' );
	else: 
		get_template_part( 'template-parts/cat', 'default' );
	endif; 
?>

<?php
get_footer();
