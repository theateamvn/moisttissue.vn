<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package moist
 */

$user = wp_get_current_user();
if ( $user->ID == 0)
    $is_logged = 0;
else
    $is_logged = 1;

/*DO REGISTER ON SUBMIT*/
if(isset($_POST['register-btn'])){

    /*GET USER POST*/
    // $username = isset($_POST['username']) ? $_POST['username'] : '';
    $username = '';
    $email = isset($_POST['email']) ? $_POST['email'] : '';
    $password = isset($_POST['password']) ? $_POST['password'] : '';
    $firstname = isset($_POST['firstname']) ? $_POST['firstname'] : '';
    $lastname = isset($_POST['lastname']) ? $_POST['lastname'] : '';
    $phone = isset($_POST['phone']) ? $_POST['phone'] : '';
    // $birthday = isset($_POST['bday']) ? $_POST['bday'] : '';
    // $address = isset($_POST['address']) ? $_POST['address'] : '';

    $user = array(
        'username' => $username,
        'email' => $email,
        'password' => $password,
        'firstname' => $firstname,
        'lastname' => $lastname,
        'phone' => $phone,
        'birthday' => '',
        'address' => '',
        'pic' => ''
    );

    $result = register_user($user);

    if($result['status']){
        if(is_page(291)){
          wp_safe_redirect(get_page_link(233));
        }else{
           wp_safe_redirect(get_permalink());
        }
    }else{
        $error_message = $result['message'];
    }

}

/*DO LOGIN ON SUBMIT*/
if(isset($_POST['login-btn'])){

    /*GET USER POST*/
    $email = isset($_POST['login_username']) ? $_POST['login_username'] : '';
    $password = isset($_POST['login_password']) ? $_POST['login_password'] : '';

    $result = userlogin($email,$password);

    if($result == 1){
        if(is_page(291)){
          wp_safe_redirect(get_page_link(233));
        }else{
           wp_safe_redirect(get_permalink());
        }
    }else{
        if($result == 'invalid_user' || $result = 'incorrect_password'){
            $error_message = 'Vui lòng kiểm tra tài khoản !';
        }else{
             $error_message = $result;
        }
    }

}

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

	<?php wp_head(); ?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-128558346-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-128558346-1');
    </script>
</head>

<body class="fadein">
    <div class="loading-bg">
        <div id="loading-icon"></div>
    </div>
    <!-- SITE WRAPPER -->
    <div id="site-wrapper" <?php if(is_page(7)){echo 'class="homepage"';} ?>>

        <!-- HEADER -->
        <header>
            <div id="menu-toggle" class="visible-xs visible-sm">
                <a href="javascript:void(0);" id="button-menu" class=""><!--active-->
                    <span></span>
                </a>
            </div>
            <nav id="main-nav">
                <div class="container">
                    <div class="logo-section">
                        <h2><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo THEME_URL; ?>/assets/images/logo.png" alt="<?php the_field('company_name', 'option'); ?>"></a></h2>
                    </div>
                    <div class="nav-section">
                        <div class="search-box search-box-mobile">
                            <?php get_search_form(); ?>
                        </div>
                        <?php
                        wp_nav_menu( array(
                            'theme_location' => 'main-menu',
                            'menu_class'        => 'menu',
                        ) );
                        ?>
                        <div class="user-nav user-nav-mobile">
                            <?php if($is_logged): ?>
                                <a href="javascript:;"><?php _e('Welcome', 'moist'); ?> <?php echo $user->display_name; ?></a> 
                                <a href="<?php echo wp_logout_url( get_permalink() ); ?>"><?php _e('Logout', 'moist'); ?></a>  
                            <?php else: ?>
                                <a href="javascript:;" class="must-log-in"><?php _e('LOGIN', 'moist'); ?></a>
                                <a href="#fblogin" onclick="fb_login();"><img src="<?php echo THEME_URL; ?>/assets/images/fb_btn.png"></a>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="contact-nav contact-nav-mobile">
                        <a href="skype:<?php the_field('skype_number', 'option'); ?>?call;"><i class="fab fa-skype"></i></a>
                        <a href="tel:<?php the_field('phone', 'option'); ?>"><i class="fas fa-phone"></i></a>
                        <a href="javascript:;" class="search-btn"><i class="fas fa-search"></i></a>
                    </div>
                </div>
            </nav>
            <div class="operation-nav">
                <div class="container">
                    <div class="o-nav-wrapper">
                        <div class="user-nav pull-left">
                            <?php if($is_logged): ?>
                                <a href="javascript:;" style="margin-top: 7px"><?php _e('Welcome', 'moist'); ?> <?php echo $user->display_name; ?></a>   
                                <a href="<?php echo wp_logout_url( get_permalink() ); ?>" style="margin-top: 7px"><?php _e('Logout', 'moist'); ?></a>   
                            <?php else: ?>
                                <a href="javascript:;" class="must-log-in"><?php _e('LOGIN', 'moist'); ?></a>
                                <a href="#fblogin" onclick="fb_login();"><img src="<?php echo THEME_URL; ?>/assets/images/fb_btn.png"></a>
                            <?php endif; ?>
                        </div>
                        <div class="contact-nav pull-right">
                            <a href="skype:<?php the_field('skype_number', 'option'); ?>?call"><i class="fab fa-skype"></i></a>
                            <a href="tel:<?php the_field('phone', 'option'); ?>"><i class="fas fa-phone"></i></a>
                             <a href="<?php the_field('store_link', 'option'); ?>" class="buynow" target="_blank"><img src="<?php echo THEME_URL; ?>/assets/images/buy_now.png"></a>
                            <a href="javascript:;" class="search-btn"><i class="fas fa-search"></i></a>
                        </div>
                        <div class="search-box">
                            <?php get_search_form(); ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            
            <div class="clearfix"></div>
        </header>
        <!-- .HEADER -->

        <!-- MAIN -->
        <main id="main-content">

