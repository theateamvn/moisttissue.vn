<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package moist
 */

get_header();
?>
	<div class="content-wrapper"> 
	    <section id="page-detail">
	        <div class="container">
	            <div class="row">
	                <div class="page-info col-sm-8">
	                    <h1 class="page-title"><?php esc_html_e( 'Requested Page Not Found!', 'moist' ); ?></h1>
	                    <p><?php esc_html_e( 'Nothing was found at this location. please try again', 'moist' ); ?></p>
	                </div>
	                <div class="col-sm-4">
	                    <?php echo get_sidebar(); ?>
	                </div>
	            </div>
	        </div>
	    </section>
	</div>

<?php
get_footer();
