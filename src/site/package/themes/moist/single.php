<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package moist
 */

get_header();
?>

<?php 
	if(in_category( 3 )): 
		get_template_part( 'template-parts/single', 'story' );
	elseif(in_category( 4 )): 
		get_template_part( 'template-parts/single', 'news' );
	else: 
		get_template_part( 'template-parts/single', 'default' );
	endif; 
?>

<?php
get_footer();
