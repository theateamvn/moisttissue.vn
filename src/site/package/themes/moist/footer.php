<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package moist
 */

?>

        </main>
        <!-- .MAIN -->

        <!-- FOOTER -->
        <?php if(!is_singular('san-pham')): ?>
        <?php 
            $args = array('posts_per_page' => -1, 'post_type' => 'san-pham', 'orderby' => 'date', 'order' => 'DESC',); 
            $posts_array = get_posts( $args ); 
        ?>
        <section id="product-slider">
            <?php if(!empty($posts_array)): ?>
            <div class="pslider-wrapper">
                <div id="pslider">
                    <?php  foreach ( $posts_array as $post ) :  setup_postdata( $post ); ?>
                    <div class="product-item">
                        <div class="item-bg" style="background: url(<?php echo get_the_post_thumbnail_url(get_the_ID(), 'md_thumb'); ?>) no-repeat center;">
                            <a href="<?php the_permalink(); ?>" class="item-link"></a>
                            
                        </div>
                        <div class="info">
                            <h4><?php the_title(); ?></h4>
                            <p><?php the_field('dong_goi'); ?><br><?php the_field('kich_thuoc'); ?></p>
                        </div>
                    </div>
                    <?php endforeach; wp_reset_postdata(); ?>
                </div>
            </div>
            <?php endif; ?>
            <div class="bottom-banner">
              <?php if(get_field('banner_footer_mobile', 'option')): ?>
                <?php if(!wp_is_mobile()): ?>
                    <?php if(get_field('banner_footer', 'option')): ?>
                    <a href="<?php the_field('link_banner_footer', 'option'); ?>"><img src="<?php the_field('banner_footer', 'option'); ?>" alt="moist"></a>
                    <?php endif; ?>
                <?php else: ?>
                    <?php if(get_field('banner_footer', 'option')): ?>
                    <a href="<?php the_field('link_banner_footer', 'option'); ?>"><img src="<?php the_field('banner_footer_mobile', 'option'); ?>" alt="moist"></a>
                    <?php endif; ?>
                <?php endif; ?>
              <?php else: ?>
                <?php if(get_field('banner_footer', 'option')): ?>
                <a href="<?php the_field('link_banner_footer', 'option'); ?>"><img src="<?php the_field('banner_footer', 'option'); ?>" alt="moist"></a>
                <?php endif; ?>
              <?php endif; ?>
            </div>
        </section>

        <?php endif; ?>

        <!-- LOGIN MODAL -->
        <?php get_template_part('template-parts/login','modal'); ?>
        <!-- END LOGIN MODAL -->

        <footer>
            <div class="footer-container">
                <div class="footer-nav">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="footer-search">
                                <?php get_template_part( 'searchform', 'footer' ) ?>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <?php
                            wp_nav_menu( array(
                                'theme_location' => 'main-menu',
                                'menu_class'        => 'footer-menu',
                            ) );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="footer-info">
                    <div class="copyright pull-left">
                        <p><?php the_field('copyright', 'option'); ?></p>
                    </div>
                    <div class="contact-link pull-right">
                        <a href="<?php the_field('facebook_page', 'option'); ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
                        <a href="mailto:<?php the_field('email', 'option'); ?>" target="_blank"><i class="fas fa-envelope"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </footer>
        
        <!-- .FOOTER -->
    </div>
    <!-- .SITE WRAPPER -->

	<?php wp_footer(); ?>
    <script type="text/javascript">
        new WOW().init();
        $(function(){
            $('#pslider').slick({
                dots: false,
                infinite: true,
                speed: 300,
                slidesToShow: 6,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 1366,
                        settings: {
                            slidesToShow: 4
                        }
                    },
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 415,
                        settings: {
                            slidesToShow: 2
                        }
                    }
                ] 
            });
        });
    </script>
    <div id="fb-root"></div>
    <script type="text/javascript">
       /*FB SIGNIN*/
       window.fbAsyncInit = function() {
           FB.init({
               appId      : '424745227713044',
               xfbml      : true,
               version    : 'v2.9'
           });
       };
       function fb_login(){
           FB.login(function(response) {
               if (response.authResponse) {
                   $('.sns-login a.fb').text('Fetching Account...');
                   access_token = response.authResponse.accessToken; //get access token
                   user_id = response.authResponse.userID; //get FB UID
                   FB.api('/me',  { fields: 'name, email, id' } ,function(response) {
                   profile_pic = 'https://graph.facebook.com/'+ response.id + '/picture?width=500';
                   
                   $data = {
                     'cmd' : 'socialLogin' ,
                     'email': response.email,
                     'name': response.name,
                     'pic' : profile_pic
                   };

                   runCommand($data,function(response) {
                     $result = $.parseJSON(response);
                     console.log($result);
                     if ($result.status == 1) {
                        window.location.reload();
                     }
                   });  

                   user_email = response.email; //get user email
                   // you can store this data into your database             
                   });
               } else {
                     //user hit cancel button
                     $('.sns-login a.fb').text('Cancelled login.');
                     console.log('User cancelled login or did not fully authorize.');
               }
           }, {
           scope: 'public_profile, email'
         });
       }
    </script>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1&appId=329179510552155&autoLogAppEvents=1';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>
