<?php 

/**
 * TEMPLATE NAME: STORY
 */

get_header();

?>

<div class="content-wrapper"> 
    <!-- STORY -->
    <section id="story-detail">
        <div class="story-bg bg-header" style="background: url(assets/images/story_bg.jpg) no-repeat center top;"></div>
        <div class="container">
            <div class="story-content wow fadeInUp">
               <?php echo wpautop(get_the_content()); ?>
            </div>
            <div class="story-info">
                <div class="row">
                    <div class="story-info-col col-md-4 wow fadeInUp">
                        <div class="col-wrapper">
                            <img src="assets/images/story1.jpg" alt="Lịch sử thương hiệu Moist">
                            <div class="content">
                                <h3>Lịch sử thương hiệu Moist</h3>
                                <p>Chúng tôi đã tích lũy được kinh nghiệm về công nghệ kỹ thuật qua một thời gian dài để có thể đáp ứng được tất cả các yêu cầu của khách hàng. Sau khi từng sản phẩm được sản xuất hàng loạt và đa dạng hóa, hiện tại chúng tôi đang mở rộng việc kinh doanh cùng với các sản </p>
                                <a class="read-more">Xem thêm &gt;&gt;</a>
                            </div>
                        </div>
                    </div>
                    <div class="story-info-col col-md-4 wow fadeInUp">
                       <div class="col-wrapper">
                            <img src="assets/images/story2.jpg" alt="Nhà sản xuất: công ty Hayashi Nhật Bản">
                           <div class="content">
                               <h3>Nhà sản xuất: công ty Hayashi Nhật Bản</h3>
                               <p>Chúng tôi đã tích lũy được kinh nghiệm về công nghệ kỹ thuật qua một thời gian dài để có thể đáp ứng được tất cả các yêu cầu của khách hàng. Sau khi từng sản phẩm được sản xuất hàng loạt và đa dạng hóa, hiện tại chúng tôi đang mở rộng việc kinh doanh cùng với các sản </p>
                               <a class="read-more">Xem thêm &gt;&gt;</a>
                           </div>
                       </div>
                    </div>
                    <div class="story-info-col col-md-4 wow fadeInUp">
                        <div class="col-wrapper">
                            <img src="assets/images/story3.jpg" alt="Nhà nhập khẩu và phân phối VN">
                            <div class="content">
                                <h3>Nhà nhập khẩu và phân phối VN</h3>
                                <p>Chúng tôi đã tích lũy được kinh nghiệm về công nghệ kỹ thuật qua một thời gian dài để có thể đáp ứng được tất cả các yêu cầu của khách hàng. Sau khi từng sản phẩm được sản xuất hàng loạt và đa dạng hóa, hiện tại chúng tôi đang mở rộng việc kinh doanh cùng với các sản </p>
                                <a class="read-more">Xem thêm &gt;&gt;</a>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        </div>
    </section>
    <!-- .STORY -->
</div>

<?php 

get_footer();