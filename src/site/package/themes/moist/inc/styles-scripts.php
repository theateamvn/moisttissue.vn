<?php
/**
 * Enqueue styles
 */

function enqueue_styles() {
	wp_enqueue_style('style-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', false, '1', 'all');
	wp_enqueue_style('style-fontawesome', 'https://use.fontawesome.com/releases/v5.2.0/css/all.css', false, '1', 'all');
	wp_enqueue_style('style-anim', THEME_URL . '/assets/css/animate.min.css', false, '1', 'all');
	// wp_enqueue_style('style-theme', THEME_URL . '/assets/css/style.css', false, '1', 'all');
	wp_enqueue_style('style-slick', THEME_URL . '/assets/css/slick.css', true, '1', 'all');
	wp_enqueue_style('style-slick-theme', THEME_URL . '/assets/css/slick-theme.css', true, '1', 'all');
	wp_enqueue_style('fancybox', THEME_URL . '/assets/css/jquery.fancybox.css', true, '1', 'all');
	wp_enqueue_style('style-theme-min', THEME_URL . '/assets/css/style.min.css', false, '1', 'all');
}
add_action('wp_enqueue_scripts', 'enqueue_styles');

/**
 * Enqueue scripts
 */
function enqueue_scripts() {
	wp_enqueue_script('script-jquery', 'https://code.jquery.com/jquery-3.1.1.min.js', array('jquery'), '1', false);
	wp_enqueue_script('script-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), '1', false);
	wp_enqueue_script('script-wow',THEME_URL . '/assets/js/inc/wow.min.js', array('jquery'), '1', true);
	// wp_enqueue_script('script-main',THEME_URL . '/assets/js/app.js', array('jquery'), '1', true);
	wp_enqueue_script('script-wow',THEME_URL . '/assets/js/inc/smart-resize.js', array('jquery'), '1', true);
	wp_enqueue_script('script-slick',THEME_URL . '/assets/js/inc/slick.min.js', array('jquery'), '1', true);
	wp_enqueue_script('script-fancybox',THEME_URL . '/assets/js/inc/jquery.fancybox.pack.js', array('jquery'), '1', true);
	wp_enqueue_script('script-validate',THEME_URL . '/assets/js/inc/jquery.validate.min.js', array('jquery'), '1', true);
	wp_enqueue_script('script-tweenmax','https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js', array('jquery'), '1', true);
	wp_enqueue_script('script-scrollto','https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/ScrollToPlugin.min.js', array('jquery'), '1', true);
	wp_enqueue_script('script-main-min',THEME_URL . '/assets/js/app.min.js', array('jquery'), '1', false);
}
add_action('wp_enqueue_scripts', 'enqueue_scripts');
