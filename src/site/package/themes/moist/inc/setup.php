<?php
/**
 * moist functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package moist
 */

if ( ! function_exists( 'moist_setup' ) ) :

	function moist_setup() {

		load_theme_textdomain( 'moist', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );

		// Define image size
		add_image_size('tn_thumb',100,100,true);		
		add_image_size('xs_thumb',200,200,true);		
		add_image_size('sm_thumb',300,300,true);		
		add_image_size('md_thumb',500,500,true);		

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'main-menu' => esc_html__( 'Top Menu', 'moist' ),
		) );
		register_nav_menus( array(
			'footer-menu' => esc_html__( 'Footer Menu', 'moist' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

	}
endif;
add_action( 'after_setup_theme', 'moist_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function moist_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'moist' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'moist' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Header Widget', 'moist' ),
		'id'            => 'header-widget',
		'description'   => esc_html__( 'Add widgets here.', 'moist' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );

}
add_action( 'widgets_init', 'moist_widgets_init' );

/**
 * Remove YOAST SEO metabox on specific post types
 */

add_action('add_meta_boxes', 'remove_yoast_metabox',11);
function remove_yoast_metabox(){
    remove_meta_box('wpseo_meta', 'slide', 'normal');
}

/********** THEME OPTION **************/

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Cài đặt Moist',
		'menu_title'	=> 'Cài đặt Moist',
		'menu_slug'	=> 'moist-settings',
		'icon_url' => 'dashicons-layout'
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Thông tin',
		'menu_title'	=> 'Thông tin',
		'parent_slug'	=> 'moist-settings',
		'menu_slug'	=> 'moist-information'
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Hiển thị chung',
		'menu_title'	=> 'Hiển thị chung',
		'parent_slug'	=> 'moist-settings',
		'menu_slug'	=> 'moist-display'
	));
	
}

/********** THEME OPTION **************/

// Add slug to menu
add_filter( 'nav_menu_css_class', 'my_special_nav_class', 10, 2 );
function my_special_nav_class( $classes, $item ) {
	if( 'page' == $item->object ){
		$page = get_post( $item->object_id );
		$classes[] = 'menu-item-' . $page->post_name;
	}

	if( 'category' == $item->object ){
        $category = get_category( $item->object_id );
        $classes[] = 'menu-item-' . $category->slug;
    }

	return $classes;
}

add_filter('redirect_canonical','lvm_disable_redirect_canonical');

function lvm_disable_redirect_canonical($redirect_url) {
    if (is_singular()) $redirect_url = false;
	return $redirect_url;
}

add_theme_support( 'editor-style' );
function wpdocs_theme_add_editor_styles() {
    add_editor_style( 'editor-style.css' );
}
add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );
 

if ( ! function_exists( 'wpex_mce_text_sizes' ) ) {
	function wpex_mce_text_sizes( $initArray ){
		$initArray['fontsize_formats'] = "9px 10px 12px 13px 14px 15px 16px 17px 18px 19px 20px 21px 22px 24px 28px 32px 36px";
		return $initArray;
	}
}

add_filter( 'tiny_mce_before_init', 'wpex_mce_text_sizes' );

remove_filter( 'the_content', 'wpautop' );
