<?php
define('SITE_URL', 	get_site_url());
define('THEME_URL', get_template_directory_uri());
define('THEME_PATH',get_template_directory());
define('LOAD_AJAX', 1);
define('HIDE_HEADER', 1);
define('HIDE_JSON', 1);
define('HIDE_EMBED',1);
define('CUSTOM_SCRIPT', 1);
define('DISABLE_ADMIN_BAR', 1);

/**
 * HIDE HEADER INFO
 */
if(HIDE_HEADER){
	remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
	remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
	remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
	remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
	remove_action( 'wp_head', 'index_rel_link' ); // index link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
	remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
	remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
}

/**
 * USE CUSTOM JS SCRIPT
 */
if(CUSTOM_SCRIPT){
	function load_custom_scripts() {
	    wp_deregister_script( 'jquery' );
	    wp_register_script('jquery', '//code.jquery.com/jquery-2.2.4.min.js', array(), '2.2.4', true); // true will place script in the footer
	    wp_enqueue_script( 'jquery' );
	}
	if(!is_admin()) {
	    add_action('wp_enqueue_scripts', 'load_custom_scripts', 99);
	}
}
if(HIDE_JSON){
	function hide_json(){
	  remove_action( 'wp_head', 'rest_output_link_wp_head' );
	  remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
	  remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
	}
	add_action( 'init', 'hide_json' );
}
if(HIDE_EMBED){
	function my_deregister_scripts(){
	  wp_deregister_script( 'wp-embed' );
	}
	add_action( 'wp_footer', 'my_deregister_scripts' );
}

/**
 * DISABLE ADMIN BAR
 */
if(DISABLE_ADMIN_BAR){
	add_filter('show_admin_bar', '__return_false');
	add_action('after_setup_theme', 'remove_admin_bar');
	function remove_admin_bar() {
	  show_admin_bar(false);
	}
}

/**
 * LOAD ADMIN AJAX
 */
if(LOAD_AJAX){
	add_action('wp_footer', 'addAjaxScript'); 
}

/**
 * CUSTOM BODY CLASS
 */
add_filter( 'body_class', 'add_slug_body_class' );
function add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}
	return $classes;
}

/**
 * HANDLE YOAST
 */
add_filter('wpseo_metabox_prio', 'yoasttobottom');
function yoasttobottom() {
	return 'low';
}

/**
 * CUSTOM PRE RESULT
 */

function pre_result($result, $exit = false){
	echo '<pre>';
	print_r($result);
	if($exit){
		die();
	}
	echo '</pre>';
}

/**
 * ============= Disable the emoji's========================
 */
function disable_emojis() {
 remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
 remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
 remove_action( 'wp_print_styles', 'print_emoji_styles' );
 remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
 remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
 remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
 remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
 add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
 add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 * 
 * @param array $plugins 
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
 if ( is_array( $plugins ) ) {
 return array_diff( $plugins, array( 'wpemoji' ) );
 } else {
 return array();
 }
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
 if ( 'dns-prefetch' == $relation_type ) {
 /** This filter is documented in wp-includes/formatting.php */
 $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

$urls = array_diff( $urls, array( $emoji_svg_url ) );
 }

return $urls;
}

/**
 * ============= END Disable the emoji's========================
 */