<?php

function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
          background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo_blue.png) !important;
      		width:253px;
      		height:101px;
      		background-size: 253px 101px;
      		background-repeat: no-repeat;
        	padding-bottom: 20px;
        }
        .login{
          background: -webkit-linear-gradient(top, rgba(154,200,251,1) 0%,rgba(238,251,255,0.24) 95%,rgba(238,251,255,0.2) 100%) !important;
          background: linear-gradient(to bottom, rgba(154,200,251,1) 0%,rgba(238,251,255,0.24) 95%,rgba(238,251,255,0.2) 100%) !important;
          filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#9ac8fb', endColorstr='#33eefbff',GradientType=0 ) !important;
        }
        .login form{
          -moz-border-radius: 10px !important;
          -ms-border-radius: 10px !important;
          -webkit-border-radius: 10px !important;
          -o-border-radius: 10px !important;
          border-radius: 10px !important;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function get_the_title_with_code($post_id){
    global $wpdb;
    $title = $wpdb->get_var( $wpdb->prepare( "SELECT post_title FROM $wpdb->posts WHERE ID = %d", $post_id));
    return $title;
}

function get_term_title_with_code($term_id){
    global $wpdb;
    $title = $wpdb->get_var( $wpdb->prepare( "SELECT name FROM $wpdb->terms WHERE term_id = %d", $term_id));
    return $title;
}

function get_the_slug( $id=null ){
  if( empty($id) ):
    global $post;
    if( empty($post) )
      return ''; // No global $post var available.
    $id = $post->ID;
  endif;

  $slug = basename( get_permalink($id) );
  return $slug;
}

/**
 * Display the page or post slug
 *
 * Uses get_the_slug() and applies 'the_slug' filter.
 */
function the_slug( $id=null ){
  echo apply_filters( 'the_slug', get_the_slug($id) );
}

function remove_image_size_attributes( $html ) {
    return preg_replace( '/(width|height)="\d*"/', '', $html );
}
 
// Remove image size attributes from post thumbnails
add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );
 
// Remove image size attributes from images added to a WordPress post
add_filter( 'image_send_to_editor', 'remove_image_size_attributes' );

/*CUSTOM LANGUAGES BAR STYLE*/
add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
    #qtranxs-meta-box-lsb {
      position: fixed;
      z-index: 999999;
      top: 0;
      right: 92px;
      background: none;
      border: none;
    } 
    #qtranxs-meta-box-lsb .postbox .handlediv{
      display: none;
    }
  </style>';
}


/**
 * Register User
 */


function register_user($user){
  global $wpdb;

  if(isset($user)){
    if((email_exists( $user['email'] )) == false){

      /*ADD NEW USER*/
      $user_id = wp_insert_user(
        array(
          'user_login'  =>  !empty($user['username']) ? $user['username'] : $user['email'],
          'user_pass' =>  $user['password'],
          'first_name'  =>  $user['firstname'],
          'last_name' =>  $user['lastname'],
          'user_nicename' =>  $user['lastname'] . '-' . $user['firstname'],
          'user_email'  =>  $user['email'],
          'display_name'  =>  $user['firstname'] . ' ' . $user['lastname'],
          'nickname'  =>  $user['firstname'] . ' ' . $user['lastname'],
          'role'    =>  'customer'
        )
      );

      if( is_wp_error( $user_id )){
        $status = array('status' => 0, 'message' => 'Error please try again');
        return $status; 
      }else{
        
        /*UPDATE USER META & BILLING INFO*/
        update_user_meta($user_id, 'birthday', $user['birthday']);
        update_user_meta($user_id, 'billing_first_name', $user['firstname']);
        update_user_meta($user_id, 'billing_last_name', $user['lastname']);
        update_user_meta($user_id, 'billing_phone', $user['phone']);
        update_user_meta($user_id, 'billing_email', $user['email']);
        update_user_meta($user_id, 'billing_address_1', $user['address']);

        if($user['pic'] != ''){
          update_user_meta($user_id, 'author_avatar', $user['pic']);
        }

        /*DO LOGIN*/
        userlogin($user['email'], $user['password']);

        $status = array('status' => 1, 'message' => 'success');
        
        return $status;
      }
    }else{
      $status = array('status' => 2, 'message' => 'This email is exists');
      return $status;
    } 
  }else{
    $status = array('status' => 0, 'message' => 'Error please try again');
    return $status;
  }
}

/**
 * Login
 */

function userlogin($userlogin, $password){
  global $wpdb;

  $result = wp_authenticate($userlogin, $password);
  if ( is_wp_error($result) ) {
    return $result->get_error_code();
  }else{
    $data = array();
    $data['user_login'] = $userlogin;
    $data['user_password'] = $password;
    $user = wp_signon( $data, false );  
    return 1;
  }
}

add_filter( 'document_title_parts', function( $title )
{
    if ( is_search() ) 
        $title['title'] = sprintf( 
            esc_html__( 'Tìm Kiếm', 'moist' ), 
            get_search_query() 
        );

    return $title;
} );