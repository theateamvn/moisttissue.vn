<?php
/**
 * Common Ajax call
 * Just send the command name in variable $_REQUEST['cmd']
 */
add_action( 'wp_ajax_nopriv_runCommand', 'runCommand' );
add_action( 'wp_ajax_runCommand', 'runCommand' );
function runCommand(){
	global $wpdb;

	if (empty($_REQUEST['cmd'])) die();

	require_once THEME_PATH . "/cmd/{$_REQUEST['cmd']}.php";

	echo $result;

	die();
}

/**
 * Add Ajax javascript to footer
 */
function addAjaxScript(){ ?>
	<script type="text/javascript">
	var yAjax = {};
	yAjax.ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

	function runCommand(data, callback, instance) {
		data.action = 'runCommand';
		instance = jQuery.ajax({
			type : "post",
			url : yAjax.ajaxurl,
			data : data,
			success: function(response) {
				callback(response);
			}
		});
	}
	</script>
<?php } 