<?php 

/**
 * TEMPLATE NAME: HOME
 */

get_header();

?>

<!-- MAIN SLIDER -->
<div id="main-slider" class="wow fadeInUp">
    <?php if(have_rows('home_slider')): ?>
        <?php while (have_rows('home_slider')): the_row(); ?>
            <div class="slide-item">
                <a href="<?php the_sub_field('link'); ?>">
                    <?php if(wp_is_mobile()): ?>
                        <?php if(get_sub_field('image_mobile')): ?>
                            <img src="<?php the_sub_field('image_mobile'); ?>" alt="<?php the_sub_field('alt'); ?>">
                        <?php  else: ?>
                            <img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('alt'); ?>">
                        <?php endif;?>
                    <?php  else: ?>
                        <img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('alt'); ?>">
                    <?php endif;?>
                    </a>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>
</div>
<!-- MAIN SLIDER -->

<!-- STORY -->
<section id="story" class="wow fadeInUp">
    <a href="<?php the_field('story_banner_link'); ?>">
        <?php if(wp_is_mobile()): ?>
            <?php if(get_field('story_banner_mobile')): ?>
                <img src="<?php the_field('story_banner_mobile'); ?>" alt="moist story">
            <?php  else: ?>
                <img src="<?php the_field('story_banner'); ?>" alt="moist story">
            <?php endif;?>
        <?php  else: ?>
            <img src="<?php the_field('story_banner'); ?>" alt="moist story">
        <?php endif;?>
    </a>
</section>
<!-- .STORY -->

<script>
    $(function(){
        
        $('#main-slider') .slick({
            dots: true,
            arrows: false,
            autoplay: true,
            speed: 1600,
            autoplaySpeed: 5000,
            fade: true,
            cssEase: 'linear',
            customPaging : function(slider, i) {
                var thumb = $(slider.$slides[i]).data();
                return '<a>'+(i+1)+'</a>';
            }
        });
        
    });
</script>


<?php 

get_footer();