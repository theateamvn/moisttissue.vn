<?php
/**
 * default search form
 */
?>
<form id="footer-search" action="<?php echo home_url('/') ?>" method="POST">
    <div class="input-group">
        <input type="text" name="s" class="form-control" placeholder="<?php esc_html_e('Enter Keywords...'); ?>">
        <button><i class="fas fa-search"></i></button>
    </div>
</form>