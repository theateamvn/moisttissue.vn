<?php
/**
 * default search form
 */
?>
<form action="<?php echo home_url('/') ?>" method="POST">
    <div class="input-wrapper">
        <input type="text" name="s" class="form-control" placeholder="<?php esc_html_e('Enter Keywords...', 'moist'); ?>">
        <button><i class="fas fa-search"></i></button>
    </div>
</form>