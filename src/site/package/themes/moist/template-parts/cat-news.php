<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package moist
 */
if ( get_query_var( 'paged' ) )
    $paged = get_query_var('paged');
else if ( get_query_var( 'page' ) )
    $paged = get_query_var( 'page' );
else
    $paged = 1;

$posts_per_page = 10;
$offset = $posts_per_page * ( $paged - 1) ;
?>
<?php 
    $args = array(
        'posts_per_page' => $posts_per_page,
        'category__in'=> 4,
        'orderby' => 'date',
        'order'   => 'DESC',
        'offset'  => $offset,
        'paged'   => $paged
    ); 
    $the_query = new WP_Query( $args );
?>
<div class="content-wrapper"> 
    <div class="news-bg" style="background: url(<?php the_field('background_news', 'option'); ?>) no-repeat center center;"></div>
    <section id="news">
        <div class="container">
            <?php if ( $the_query->have_posts() ): ?>
                <div class="news-list">
                    <div class="row">   
                    <?php  while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <div class="news-item col-sm-6 wow fadeInUp">
                        <div class="featured-bg" style="background: url(<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>) no-repeat center">
                            <a href="<?php the_permalink(); ?>" class="item-link"></a>
                        </div>
                    </div>
                    <?php endwhile; ?>
                    </div>
                </div>
                <?php  if($the_query->max_num_pages >= 2): ?>
                <nav id="pagination" class="clear text-center">
                    <?php

                    global $wp_query;

                    $big = 999999999; // need an unlikely integer

                    echo paginate_links( array(
                        'base' => @add_query_arg('page','%#%'),
                        'format' => 'page/%#%/',
                        'current' => $paged,
                        'prev_text'          => __('Previous'),
                        'next_text'          => __('Next'),
                        'total' => $the_query->max_num_pages
                    ) );
                    ?>
                </nav>
                <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </section>
</div>