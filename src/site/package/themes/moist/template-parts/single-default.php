<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package moist
 */

?>

<div class="content-wrapper"> 
    <!-- STORY -->
    <section id="story-detail">
        <div class="story-bg bg-header" style="background: url(<?php the_field('background_story', 'option'); ?>) no-repeat center top;"></div>
        <div class="container">
          <div class="story-content wow fadeInUp">
             <h1><?php the_title(); ?></h1>
             <?php echo wpautop(get_the_content()); ?>
          </div>
        </div>
    </section>
    <!-- .STORY -->
</div>
