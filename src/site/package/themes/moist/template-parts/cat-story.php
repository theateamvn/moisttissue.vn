<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package moist
 */

if ( get_query_var( 'paged' ) )
    $paged = get_query_var('paged');
else if ( get_query_var( 'page' ) )
    $paged = get_query_var( 'page' );
else
    $paged = 1;

$posts_per_page = 9;
$offset = $posts_per_page * ( $paged - 1) ;
$args = array(
    'posts_per_page' => $posts_per_page,
    'category__in'=> 3,
    'orderby' => 'date',
    'order'   => 'DESC',
    'offset'  => $offset,
    'paged'   => $paged
); 
$the_query = new WP_Query( $args );
?>

<div class="content-wrapper"> 
    <!-- STORY -->
    <section id="story-detail">
        <div class="story-bg bg-header" style="background: url(<?php the_field('background_story', 'option'); ?>) no-repeat center top;"></div>
        <div class="container">
        	<?php if ( $the_query->have_posts() ): ?>
            <div class="story-info">
                <div class="row">
                	<?php  while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <div class="story-info-col col-md-4 wow fadeInUp">
                        <div class="col-wrapper">
                            <a href="<?php the_permalink(); ?>"><img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'md_thumb'); ?>" alt="<?php the_title(); ?>"></a>
                            <div class="content">
                               <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                               <p><?php echo mb_strimwidth(get_the_excerpt(), 0, 200,  '...'); ?></p>
                               <a href="<?php the_permalink(); ?>" class="read-more"><?php _e('Read More', 'moist'); ?> &gt;&gt;</a>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
               </div>
               <?php  if($the_query->max_num_pages >= 2): ?>
               <nav id="pagination" class="clear text-center">
                   <?php

                   global $wp_query;

                   $big = 999999999; // need an unlikely integer

                   echo paginate_links( array(
                       'base' => @add_query_arg('page','%#%'),
                       'format' => 'page/%#%/',
                       'current' => $paged,
                       'prev_text'          => __('Previous'),
                       'next_text'          => __('Next'),
                       'total' => $the_query->max_num_pages
                   ) );
                   ?>
               </nav>
               <?php wp_reset_postdata(); ?>
               <?php endif; ?>
       		</div>
       		<?php endif; ?>
        </div>
    </section>
    <!-- .STORY -->
</div>
