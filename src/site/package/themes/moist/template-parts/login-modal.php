<div id="login-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"><i class="fas fa-user"></i></i>
                    <?php _e('Account','moist'); ?></h4>
            </div>
            <div class="form-wrapper">
                <?php if(!empty($error_message)): ?>
                <div class="alert alert-danger">
                  <span><?php echo $error_message; ?></span>
                </div>
                <?php endif; ?>
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 form-col">
                            <a href="javascript:void(0);" class="login-btn form-btn extra-padding active" ><?php _e('Login','moist'); ?></a>
                        </div>
                        <div class="col-xs-12 col-sm-6 form-col">
                            <a href="javascript:void(0);" class="register-btn form-btn extra-padding"><?php _e('Create new account','moist'); ?></a>
                        </div>
                    </div>
                </div>
                <form id="login-form" action="" method="POST">
                    <div class="form-group">
                        <div class="section-header">
                            <h3 class="section-title"><?php _e('Login','moist'); ?></h3>
                        </div>
                        <div class="form-group">
                            <input type="text" name="login_username" id="login_username" class="form-control no-border-input" placeholder="<?php _e('Email / Username','moist'); ?> *"" />
                        </div>
                        <div class="form-group">
                            <input type="password" name="login_password" id="login_password" class="form-control no-border-input" placeholder="<?php _e('Password','moist'); ?> *" />
                        </div>
                        <div class="form-group lg-spacing-top">
                            <button type="submit" name="login-btn" class="btn btn-default"><?php _e('Login','moist'); ?> &nbsp <i class="fa fa-caret-right" aria-hidden="true"></i></button>
                        </div>
                        <div class="info-link text-center">
                            <!-- <a href="#" class="form-info green-color bold-text"><?php //_e('Quên mật khẩu?','moist'); ?></a> -->
                            <p><?php _e('No account?','moist'); ?> <a href="javascript:;" class="lt-green-color bold-text register-btn"><?php _e('Register','moist'); ?></a> <?php _e('now','moist'); ?></p>
                        </div>
                    </div>
                </form>
                <form id="register-form" action="" method="POST" style="display: none;">
                    <div class="form-group">
                        <div class="section-header">
                            <h3 class="section-title"><?php _e('Register','moist'); ?></h3>
                        </div>
                        <div class="form-group">
                            <input type="text" name="email" id="email" class="form-control no-border-input" placeholder="<?php _e('Email login','moist'); ?> *"" />
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" id="password" class="form-control no-border-input" placeholder="<?php _e('Password','moist'); ?> *" />
                        </div>
                        <div class="form-group">
                            <input type="password" name="confirm_password" id="confirm_password" class="form-control no-border-input" placeholder="<?php _e('Confirm Password','moist'); ?> *" />
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-6">
                                <input type="text" name="lastname" id="lastname" class="form-control no-border-input" placeholder="<?php _e('Last Name','moist'); ?> *"" />
                            </div>
                            <div class="form-group col-xs-12 col-sm-6">
                                <input type="text" name="firstname" id="firstname" class="form-control no-border-input" placeholder="<?php _e('First Name','moist'); ?> *"" />
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" name="phone" id="phone" class="form-control no-border-input" placeholder="<?php _e('Phone Number','moist'); ?> *"" />
                        </div>
                       
                        <div class="form-group lg-spacing-top">
                            <button type="submit" name="register-btn" class="btn btn-default"><?php _e('Register','moist'); ?> &nbsp <i class="fa fa-caret-right" aria-hidden="true"></i></button>
                        </div>
                        <div class="info-link text-center">
                            <p class="form-info"> <?php _e('Already member of ','moist'); ?><span class="bold-text green-color">Moist Vietnam</span></p>
                            <p><?php _e("Let's",'moist'); ?> <a href="javascript:void" class="lt-green-color bold-text login-btn"><?php _e('Login','moist'); ?></a> <?php _e('now','moist'); ?></p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>