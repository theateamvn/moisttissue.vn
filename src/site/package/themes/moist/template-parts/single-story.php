<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package moist
 */

?>

<div class="content-wrapper"> 
    <!-- STORY -->
    <section id="story-detail">
        <div class="story-bg bg-header" style="background: url(<?php the_field('background_story', 'option'); ?>) no-repeat center top;"></div>
        <div class="container">
          <div class="story-content wow fadeInUp">
             <h1><?php the_title(); ?></h1>
             <?php echo wpautop(get_the_content()); ?>
          </div>
          <?php 
          $args = array('posts_per_page' => 3, 'category' => 3, 'orderby' => 'date', 'order' => 'DESC',); 
          $posts_array = get_posts( $args ); 
          ?>
        	 <?php if(!empty($posts_array)): ?>
            <div class="story-info">
                <div class="row">
                	<?php  foreach ( $posts_array as $post ) :  setup_postdata( $post ); ?>
                    <div class="story-info-col col-md-4 wow fadeInUp">
                        <div class="col-wrapper">
                            <a href="<?php the_permalink(); ?>"><img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'md_thumb'); ?>" alt="<?php the_title(); ?>"></a>
                            <div class="content">
                               <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                               <p><?php echo mb_strimwidth(get_the_excerpt(), 0, 200,  '...'); ?></p>
                               <a href="<?php the_permalink(); ?>" class="read-more"><?php _e('Xem thêm', 'moist'); ?> &gt;&gt;</a>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; wp_reset_postdata(); ?>
               </div>
       		</div>
       		<?php endif; ?>
        </div>
    </section>
    <!-- .STORY -->
</div>
