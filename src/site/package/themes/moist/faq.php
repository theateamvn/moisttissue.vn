<?php
/**
 * TEMPLATE NAME: FAQ
 */
/*GET CURRENT USER*/
$user = wp_get_current_user();
$is_logged = 0;
$check = 0;
$error_message = '';
$per_page = 12;

if ( $user->ID == 0)
    $is_logged = 0;
else
    $is_logged = 1;

if(isset($_POST['question'])){
    /*GET POST DATA*/
    $name = $user->display_name;
    $question = isset($_POST['question']) ? $_POST['question'] : '';

    if(empty($question)){
        $error_message = 'Vui lòng nhập nội dung câu hỏi!';
    }else{         
        /*INSERT FEEDBACK*/
        $check = wp_insert_post(
            array(
                'post_title'=> 'Câu hỏi từ ' . $name,
                'post_type'=>'faq',
                'post_content'=>$question,
                'post_status' => 'pending',
                'post_excerpt' => $name
            )
        );
    }
}

get_header();
?>

<div class="content-wrapper"> 
    <div class="faq-bg" style="background: url(<?php the_field('faq_background'); ?>) no-repeat center top;"></div>
    <div class="faq-search-nav">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 search-group related-search">
                    <form action="" method="POST">
                        <label for="related-s" class="form-label"><?php _e('Similar question:', 'moist'); ?></label><!--
                    --><input type="text" name="skeyword" placeholder="<?php esc_html_e('Enter your question.....', 'moist') ?>">
                    </form>
                </div>
                <div class="col-sm-6 search-group keyword-search">
                    <?php if($is_logged): ?>
                    <form action="" method="POST">
                        <label for="question" class="form-label"><?php _e('Your question:', 'moist'); ?></label><!--
                    --><textarea name="question" class="question-input"  placeholder="<?php esc_html_e('Enter your question.....', 'moist') ?>"></textarea><!--
                    --><input type="image" src="<?php echo THEME_URL; ?>/assets/images/send_btn.png" name="submit-question" class="send-question" alt="Submit" width="48" height="48">
                        <a href="javascript:;" class="close-form"><i class="far fa-times-circle"></i></a>
                    </form>
                    <?php else: ?>
                    <label class="login-label"><?php _e('Please', 'moist'); ?> <a href="javascript:;" class="must-log-in"><?php _e('Login', 'moist'); ?></a> <?php _e('to send question', 'moist'); ?></label>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div id="faq-content">
            <?php if(!empty($error_message)): ?>
            <div class="alert alert-danger">
              <span><?php echo $error_message; ?></span>
            </div>
            <?php endif; ?>
            <a href="<?php echo esc_url( add_query_arg( 'limit', '0', get_page_link(125) )  ); ?>" class="view-all"><?php _e('View All Question:', 'moist'); ?> </a>
            <?php 
            if ( get_query_var( 'paged' ) )
                $paged = get_query_var('paged');
            else if ( get_query_var( 'page' ) )
                $paged = get_query_var( 'page' );
            else
                $paged = 1;

            $posts_per_page = isset($_GET['limit']) ? 30 : 10;
            $offset = $posts_per_page * ( $paged - 1) ;
            $keyword = isset($_POST['skeyword']) ? $_POST['skeyword'] : ''; 
            $args = array(
                'posts_per_page' => $posts_per_page,
                'post_type' => 'faq',
                's' => $keyword,
                'orderby' => 'date',
                'order'   => 'DESC',
                'offset'  => $offset,
                'paged'   => $paged
            ); 
            $the_query = new WP_Query( $args );
            $first = 1; 
            $delay = 0.1;
            ?>
            <?php if(!empty($keyword)): ?>
            <h3><?php printf( esc_html__( 'Question with keyword: %s', 'moist' ),  $keyword ); ?></h3>
            <?php endif; ?>
            <?php if ( $the_query->have_posts() ): ?>

                <?php  while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <div class="faq-item wow fadeInUp" data-wow-delay="<?php echo $delay; ?>s">
                    <?php if($first): $first = 0; ?>
                    <h3><?php _e('Latest question', 'moist') ?></h3>
                    <?php endif; ?>
                    <div class="q">
                        <div class="row">
                            <div class="left-col">
                                <div class="avatar" style="background: url(<?php echo THEME_URL; ?>/assets/images/sample.png) no-repeat center;"></div>
                            </div>
                            <div class="right-col">
                                <h4 class="name"><?php echo get_the_excerpt(); ?></h4>
                                <p class="question"><?php the_content(); ?> </p>
                            </div>
                        </div>
                    </div>
                    <div class="a">
                        <div class="row">
                            <div class="left-col">
                                <h5><?php _e('Answer :', 'moist'); ?></h5>
                            </div>
                            <div class="right-col">
                                <p class="answer">
                                    <?php the_field('answer'); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $delay += 0.1; ?>
                <?php endwhile; ?>
            <?php else: ?>
            <p><?php _e('No question found', 'moist'); ?>i</p>
            <?php endif; ?>

            <?php  if($the_query->max_num_pages >= 2): ?>
            <nav id="pagination" class="clear text-center">
                <?php

                global $wp_query;

                $big = 999999999; // need an unlikely integer

                echo paginate_links( array(
                    'base' => @add_query_arg('page','%#%'),
                    'format' => 'page/%#%/',
                    'current' => $paged,
                    'prev_text'          => __('Previous'),
                    'next_text'          => __('Next'),
                    'total' => $the_query->max_num_pages
                ) );
                ?>
            </nav>
            <?php wp_reset_postdata(); ?>
            <?php endif; ?>

        </div>
    </div>
</div>
<?php if($check != 0): ?>
<input type="hidden" id="url" value="<?php echo get_page_link(125); ?>">
<div id="success-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"><i class="fa fa-question" aria-hidden="true"></i>
                    <?php _e('Your question has been sent', 'moist'); ?></h4>
            </div>
            <div class="modal-info text-center">
                <h4><i class="fa fa-check" aria-hidden="true"></i> <?php _e('You have just sent your question successfully to Moist Vietnam! ','moist'); ?> </h4>
                <p><?php _e('We will contact you soon!','moist') ?></p>
                <a href="javascript:;" class="close-modal-btn site-link-btn"><i class="fa fa-angle-right" aria-hidden="true"></i> <?php _e('Done','moist'); ?></a>
            </div>    
        </div>
    </div>
</div>            
<script type="text/javascript">
    $(function(){
        $('#success-modal').modal('show');
        $('.close-modal-btn').on('click',function(){
            $('#success-modal').modal('hide');
        });
        $('#success-modal').on('hidden.bs.modal', function () {
            window.location.replace($('#url').val());
        })
    })
</script>   
<?php endif; ?>
<script type="text/javascript">
    $(function(){
        var ww = $(window).width();
        if(ww > 768){
            $('.question-input').focusin(function(){
                $('.faq-search-nav').addClass('question-active');
                $('.faq-search-nav').stop().fadeOut().stop().fadeIn();
            });
            $('.close-form').on('click', function(){
                $('.faq-search-nav').removeClass('question-active');
                $('.faq-search-nav').stop().fadeOut().stop().fadeIn();
            });
        }
    })
</script>
<?php

get_footer();
