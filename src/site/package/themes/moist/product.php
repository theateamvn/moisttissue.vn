<?php
/**
 * TEMPLATE NAME: PRODUCTS
 */

get_header();
?>
<?php 
    if ( get_query_var( 'paged' ) )
        $paged = get_query_var('paged');
    else if ( get_query_var( 'page' ) )
        $paged = get_query_var( 'page' );
    else
        $paged = 1;

    $posts_per_page = 10;
    $offset = $posts_per_page * ( $paged - 1) ;
    $args = array(
        'posts_per_page' => $posts_per_page,
        'post_type' => 'san-pham',
        'orderby' => 'date',
        'order'   => 'DESC',
        'offset'  => $offset,
        'paged'   => $paged
    ); 

    $the_query = new WP_Query( $args );
?>
<!-- PRODUCTS -->
<section id="products">
    <div class="container">
        <?php if ( $the_query->have_posts() ): ?>
            <div class="product-list" itemscope itemtype="http://schema.org/Product">
                <?php $delay = 0.1; ?>
                <?php  while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <div class="product-item wow fadeInUp" data-wow-delay="<?php echo $delay; ?>s">
                    <div class="row">
                        <div class="product-image col-sm-2">
                            <a href="<?php the_permalink(); ?>">
                                <img itemprop="image" src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'md_thumb'); ?>">
                            </a>
                        </div>
                        <div class="product-info col-sm-10">
                            <h3 itemprop="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <h4><?php echo get_the_excerpt(); ?></h4>
                            <?php if(get_field('dong_goi')): ?>
                            <p class="description">
                               <?php _e('Package:','moist'); ?> <?php the_field('dong_goi'); ?>
                            </p>
                            <?php endif;?>
                            <?php if(get_field('kich_thuoc')): ?>
                            <p class="description">
                                <?php _e('Dimension:','moist'); ?> <?php the_field('kich_thuoc'); ?>
                            </p>
                            <?php endif;?>
                            <?php if(get_field('so_hop_quy')): ?>
                            <p class="description">
                                <?php _e('Number Code:','moist'); ?> <?php the_field('so_hop_quy'); ?>
                            </p>
                            <?php endif;?>
                            <p class="more-detail" itemprop="description">
                                <?php the_field('more_detail'); ?>
                            </p>
                            <div class="rating">
                                <?php echo do_shortcode( '[ratings]' ); ?>
                            </div>
                            <a href="<?php the_permalink(); ?>" class="plink"><?php _e('product information', 'moist'); ?></a>
                        </div>
                    </div>
                    <!-- <a href="#" class="buy-btn"><img src="assets/images/buy_btn.png"></a> -->
                    <a href="<?php the_field('product_link'); ?>" class="buy-btn buy-btn-custom"><?php _e('BUY NOW', 'moist') ?><i class="fas fa-shopping-cart"></i></a>
                </div>
                <?php $delay += 0.15; ?>
                <?php endwhile; ?>
            </div>
            <?php  if($the_query->max_num_pages >= 2): ?>
            <nav id="pagination" class="clear text-center">
                <?php

                global $wp_query;

                $big = 999999999; // need an unlikely integer

                echo paginate_links( array(
                    'base' => @add_query_arg('page','%#%'),
                    'format' => 'page/%#%/',
                    'current' => $paged,
                    'prev_text'          => __('Previous'),
                    'next_text'          => __('Next'),
                    'total' => $the_query->max_num_pages
                ) );
                ?>
            </nav>
            <?php wp_reset_postdata(); ?>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</section>
<!-- .PRODUCTS -->

<?php
get_footer();
