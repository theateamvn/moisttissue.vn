<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package moist
 */

get_header();
?>
<div class="content-wrapper"> 
    <section id="page-detail">
        <div class="container">
            <div class="row">
                <div class="page-info col-sm-8">
                    <?php echo wpautop(get_the_content()); ?>
                </div>
                <div class="col-sm-4">
                    <?php echo get_sidebar(); ?>
                </div>
            </div>
        </div>
    </section>
</div>

<?php

get_footer();
