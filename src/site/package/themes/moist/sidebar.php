<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package moist
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<a href="<?php the_field('link_banner_sidebar', 'option') ?>"><img src="<?php the_field('banner_sidebar', 'option') ?>"></a>
