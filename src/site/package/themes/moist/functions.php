<?php

include_once 'inc/common.php';
include_once 'inc/setup.php';
include_once 'inc/custom.php';
include_once 'inc/styles-scripts.php';
include_once 'inc/ajax.php';

add_action( 'admin_menu', 'dvpz_remove_admin_menu', 99999 );
function dvpz_remove_admin_menu() {
	if(get_current_user_id() != 1){
		remove_menu_page( 'plugins.php' );
		remove_menu_page( 'edit.php?post_type=acf-field-group' );
		remove_menu_page( 'itsec' );
		remove_menu_page( 'toolset-dashboard' );
		remove_menu_page( 'tools.php' );
		remove_menu_page( 'edit-comments.php' );
		remove_submenu_page( 'options-general.php', 'duplicatepost' );
		remove_submenu_page( 'options-general.php', 'options-permalink.php' );
		remove_submenu_page( 'options-general.php', 'options-media.php' );
		remove_submenu_page( 'options-general.php', 'options-discussion.php' );
		remove_submenu_page( 'themes.php', 'customize.php' );
		remove_submenu_page( 'themes.php', 'widgets.php' );
	}
}