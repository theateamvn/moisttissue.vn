<?php
/**
 * The template for displaying all single product
 *
 * @package moist
 */

get_header();
?>

<div class="content-wrapper"> 
    <!-- PRODUCT DETAIL -->
    <section id="product-detail">
        <div class="product-info" itemscope itemtype="http://schema.org/Product">
            <div class="container">
                <div class="row">
                    <div class="product-image col-sm-4">
                        <img itemprop="image" src="<?php the_field('detail_image'); ?>" alt="<?php the_title(); ?>">
                    </div><!--
                --><div class="product-info-detail col-sm-8">
                        <div class="wrapper">
                            <h1 itemprop="name"><?php the_title(); ?></h1>
                            <h5><?php the_field('dong_goi'); ?><br><?php the_field('kich_thuoc'); ?></h5>
                            <div class="rating">
                               <?php echo do_shortcode( '[ratings]' ); ?>
                            </div>
                            <div itemprop="description">
                            <?php echo wpautop(get_the_content()); ?>
                            </div>
                            <a href="<?php the_field('product_link'); ?>" class="buy-btn" target="_blank"><img src="<?php echo THEME_URL; ?>/assets/images/buy_btn.png"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product-featured text-center">
            <div class="featured-header">
                <img src="<?php echo THEME_URL; ?>/assets/images/detail_header.png">
                <h3><?php _e('Product Detail', 'moist'); ?></h3>
            </div>
 
            <?php if(!have_rows('product_detail_mobile')): ?>

            	<?php if(have_rows('product_detail')): ?>
            		<?php while(have_rows('product_detail')): the_row(); ?>
            			<img src="<?php the_sub_field('banner'); ?>" class="xxl-spacing-btm">
        			<?php endwhile; ?>
        		<?php endif; ?>

            <?php else: ?>

            	<?php if(!wp_is_mobile()): ?>

            		<?php if(have_rows('product_detail')): ?>
	            		<?php while(have_rows('product_detail')): the_row(); ?>
	            			<img src="<?php the_sub_field('banner'); ?>" class="xxl-spacing-btm">
	        			<?php endwhile; ?>
        			<?php endif; ?>

            	<?php else: ?>

            		<?php if(have_rows('product_detail_mobile')): ?>
	            		<?php while(have_rows('product_detail_mobile')): the_row(); ?>
	            			<img src="<?php the_sub_field('banner'); ?>" class="xxl-spacing-btm">
	        			<?php endwhile; ?>
					<?php endif; ?>

        		<?php endif; ?>

            <?php endif; ?>	

        </div>
    </section>
    <!-- .PRODUCT DETAIL -->
    <section id="product-slider" class="related-product">
        <div class="container">
            <h3><?php _e('Other Products', 'moist') ?></h3>
            <?php 
                $args = array('posts_per_page' => -1, 'post_type' => 'san-pham','exclude' => get_the_ID(), 'orderby' => 'date', 'order' => 'DESC',); 
                $posts_array = get_posts( $args ); 
            ?>
            <?php if(!empty($posts_array)): ?>
            <div class="pslider-wrapper">
                <div id="pslider">
                    <?php  foreach ( $posts_array as $post ) :  setup_postdata( $post ); ?>
                    <div class="product-item">
                        <div class="item-bg" style="background: url(<?php echo get_the_post_thumbnail_url(get_the_ID(), 'md_thumb'); ?>) no-repeat center;">
                            <a href="<?php the_permalink(); ?>" class="item-link"></a>
                            
                        </div>
                        <div class="info">
                            <h4><?php the_title(); ?></h4>
                            <p><?php the_field('dong_goi'); ?><br><?php the_field('kich_thuoc'); ?></p>
                        </div>
                    </div>
                    <?php endforeach; wp_reset_postdata(); ?>
                </div>
            </div>
            <?php endif; ?>
            <div class="bottom-banner">
              <?php if(get_field('banner_footer_mobile', 'option')): ?>
                <?php if(!wp_is_mobile()): ?>
                    <?php if(get_field('banner_footer', 'option')): ?>
                    <a href="<?php the_field('link_banner_footer', 'option'); ?>"><img src="<?php the_field('banner_footer', 'option'); ?>" alt="moist"></a>
                    <?php endif; ?>
                <?php else: ?>
                    <?php if(get_field('banner_footer', 'option')): ?>
                    <a href="<?php the_field('link_banner_footer', 'option'); ?>"><img src="<?php the_field('banner_footer_mobile', 'option'); ?>" alt="moist"></a>
                    <?php endif; ?>
                <?php endif; ?>
              <?php else: ?>
                <?php if(get_field('banner_footer', 'option')): ?>
                <a href="<?php the_field('link_banner_footer', 'option'); ?>"><img src="<?php the_field('banner_footer', 'option'); ?>" alt="moist"></a>
                <?php endif; ?>
              <?php endif; ?>
            </div>
        </div>
    </section>
</div>

<?php
get_footer();
